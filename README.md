# Elecex Coding Challenge 1

Thank you for your interest in working at Electricity Exchange. Before we proceed with more formal interviews, we ask that all candidates submit a coding challenge. The coding challenge is a foundational piece of our process and it's referenced later in our process during the technical interviews.

This challenge is intended to assess your coding ability in NodeJS.

You have one week to complete this challenge.

## Prerequisites

- [NodeJS Installed](https://nodejs.org/en/download/)

## Available Commands

From the root directory there are a few commands that you can run

- _npm install_ - Install all dependencies.
- _npm start_ - Run the project.
- _npm run test_ - Run the test suite.

## Project Background

This project contains the beginnings of a simple REST api.

It consists of an SQLite database with two tables (Sites, Devices), and a simple webserver that currently exposes three RESTful endpoints.

You are expected to implement some new functionality, and ensure this new functionality is tested.

## Requirements

- Implement the endpoint that will make the tests defined in `test/tests/sites.spec.js` pass.
- Add an endpoint to retrieve all active devices.
- Update all new and existing endpoints to verify that parameters received are of the correct type & valid, adding any tests you feel are appropriate.

- Create a front end using Vue.JS, which has the following capabilities.
-     Display the number of devices.
-     Display details of the devices 
-     Add a new device
-     Delete a device by passing an id. 
-     The front end should poll or be pushed from the API to ensure it’s refreshed every 30 seconds.


If you can think of any other changes that would improve the project, feel free to outline them in the improvements.txt file in this repo.

## Assessment

Your submission will be reviewed on the following points:

- Your ability to follow the requirements
- Implementation of the requirements
- Code readability and maintainability
- Test quality and coverage

## Project Overview

### Application Codebase

- src/index.js
    This file initialises the webserver and database and starts the server listening on localhost:3000
- src/lib/Database.js
    This file is responsible for initialising the database & ensuring migrations are applied.

### Testware

- test/util/httpClient.js
    This file exposes a utility for making HTTP requests to the API when running your tests.
- test/tests/*.spec.js
    These files contain the integration tests for the implemented REST endpoints.

## Submission

To submit your coding challenge, commit all your changes to the `master` branch and run the following command:

```git bundle create coding-challenge.bundle HEAD master```

Email your ```coding-challenge.bundle``` file to the person that originally sent you this coding challenge. We do our best to review and respond to submissions within 5 business days.

Best of luck, and thanks for taking the time to do this coding challenge.