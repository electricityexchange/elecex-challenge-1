'use strict';

const sqlite = require('sqlite');
const sqlite3 = require('sqlite3');
const path = require('path');

const filename = path.resolve(__dirname, 'database.sqlite');

module.exports = function () {
    return {
        async init() {
            const db = await sqlite.open({ filename, driver: sqlite3.Database });
            await db.migrate({ force: 'last' });
            return db;
        }
    };
};
